﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class CharacterActions : MonoBehaviour
{
    public int m_PlayerNumber;
    public float m_JumpMagnitude = 1f;

    private Animator m_Animator;
    private Rigidbody m_Rigidbody;
    private float m_ForwardSpeed;
    private bool m_IsJumping = false;
    private List<GameObject> m_HitParts = new List<GameObject>();        // keep a reference for own hit parts



    // Start is called before the first frame update
    void Awake()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
  
    }

    private void OnEnable()
    {
        m_Rigidbody.isKinematic = false;
        
        // inverse player 2 walk animation 
        if (m_Animator && m_PlayerNumber == 2)
        {
            m_Animator.SetFloat("Player2Inverse", -1);
        }

        foreach (GameObject part in GameObject.FindGameObjectsWithTag("HitPart"))
        {
            part.GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    void Start()
    {
        // Disarm all the parts that harms people..
        
        foreach (GameObject part in GameObject.FindGameObjectsWithTag("HitPart"))
        {

            if (part.transform.root.GetComponent<CharacterActions>().m_PlayerNumber == m_PlayerNumber)
            {
                m_HitParts.Add(part);
                part.GetComponent<CapsuleCollider>().enabled = false;
            }

        }
    }

    private void OnDisable()
    {
        m_Rigidbody.isKinematic = true;
    }

    private void OnAnimatorMove()
    {
        // Move player when moving happens
        if (m_Animator && (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("walk") || m_Animator.GetCurrentAnimatorStateInfo(0).IsName("backward") || m_Animator.GetCurrentAnimatorStateInfo(0).IsName("jump")))
        {
            Vector3 movement = new Vector3();

            movement.x += m_Animator.GetFloat("ForwardSpeed") * Time.deltaTime;

            float leftLockPosition = GameObject.Find("StageBorderLeft").transform.position.x;
            float rightLockPosition = GameObject.Find("StageBorderRight").transform.position.x;

            if (m_Rigidbody.position.x + movement.x > leftLockPosition && m_Rigidbody.position.x + movement.x < rightLockPosition)
            {
                m_Rigidbody.MovePosition(m_Rigidbody.position + movement);
            } else
            {
                if (Math.Abs(m_Rigidbody.position.x - leftLockPosition) < Math.Abs(m_Rigidbody.position.x - rightLockPosition))
                {
                    m_Rigidbody.MovePosition(new Vector3(leftLockPosition, m_Rigidbody.position.y, m_Rigidbody.position.z));
                }
                else
                {
                    m_Rigidbody.MovePosition(new Vector3(rightLockPosition, m_Rigidbody.position.y, m_Rigidbody.position.z));
                } 
            }
            
        }
        // Elevates player when Jumping happens
        if (m_Animator && m_Animator.GetCurrentAnimatorStateInfo(0).IsName("jump") && !m_IsJumping)
        {
            m_IsJumping = true;
            Vector3 jumpForce = Vector3.Normalize(Vector3.up);
            m_Rigidbody.AddForce(jumpForce * m_JumpMagnitude);
        }
        // Reset the Jumping states to false
        if (m_Animator && !m_Animator.GetCurrentAnimatorStateInfo(0).IsName("jump"))
        {
            m_IsJumping = false;
        }

        // Move back player a little bit when got hit
        if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("hit"))
        {
            float leftLockPosition = GameObject.Find("StageBorderLeft").transform.position.x;
            float rightLockPosition = GameObject.Find("StageBorderRight").transform.position.x;

            Vector3 newPosition = m_Rigidbody.position + Vector3.right * -1 * m_Animator.GetFloat("Player2Inverse") * Time.deltaTime;

            if (newPosition.x > leftLockPosition && newPosition.x < rightLockPosition)
            {
                m_Rigidbody.MovePosition(newPosition);
            }
                     
        }

        if (m_Animator.GetCurrentAnimatorStateInfo(0).IsName("death"))
        {
            m_Animator.SetBool("Death", false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (m_Animator)
        {
            Walk();
            Crouch();
            Block();
            Jump();
            Punch();
            Throw();
            Taunt();

        }

    }

    private void Punch()
    {
        if (Input.GetButtonDown("Punch" + m_PlayerNumber))
        {
            m_Animator.SetTrigger("Punch");           
        }
    }

    private void Jump()
    {
        if (Input.GetAxis("Vertical" + m_PlayerNumber) < -0.1f)
        {
            m_Animator.SetTrigger("Jump");

        }
    }

    private void Throw()
    {
        if (Input.GetButtonDown("Throw" + m_PlayerNumber))
        {
            m_Animator.SetTrigger("Throw");

        }
    }

    private void Crouch()
    {

        if (Input.GetAxis("Vertical" + m_PlayerNumber) > 0.1f)
        {
            m_Animator.SetBool("Crouch", true);
        }
        if (Input.GetAxis("Vertical" + m_PlayerNumber) < 0.09f && Input.GetAxis("Vertical" + m_PlayerNumber) > -0.09f)
        {
            m_Animator.SetBool("Crouch", false);
        }

    }

    private void Block()
    {

        if (Input.GetButtonDown("Block" + m_PlayerNumber))
        {
            m_Animator.SetBool("Block", true);
        }
        if (Input.GetButtonUp("Block" + m_PlayerNumber))
        {
            m_Animator.SetBool("Block", false);
        }

    }

    private void Taunt()
    {

        if (Input.GetButtonDown("Taunt" + m_PlayerNumber))
        {
            m_Animator.SetTrigger("Taunt");
        }

    }

    private void Walk()
    {
        m_ForwardSpeed = Input.GetAxis("Horizontal" + m_PlayerNumber);
        m_Animator.SetFloat("ForwardSpeed", m_ForwardSpeed);

    }
    // Expose function for collision to call
    public void GotHit()
    {
        m_Animator.SetTrigger("Hit");
    }

    public void OnDeath()
    {
        m_Animator.SetBool("Death", true);
    }

    public void DisableHitPart()
    {
        foreach (GameObject part in m_HitParts)
        {
            part.GetComponent<CapsuleCollider>().enabled = false;
        }
    }

    public void EnableHitPart()
    {
        foreach (GameObject part in m_HitParts)
        {
            part.GetComponent<CapsuleCollider>().enabled = true;
        }
    }

    public void ThrowProjectile(AnimationEvent animationEvent)
    {
        GameObject projectile = Instantiate(animationEvent.objectReferenceParameter as GameObject, gameObject.transform.GetChild(1).transform.position, gameObject.transform.rotation, gameObject.transform);
        projectile.GetComponent<ConstantForce>().force = new Vector3( 4 * gameObject.GetComponent<Animator>().GetFloat("Player2Inverse"), 0, 0);
        projectile.GetComponent<ConstantForce>().torque = new Vector3( 3 * gameObject.GetComponent<Animator>().GetFloat("Player2Inverse"), 0, 0);
        
    }
}
