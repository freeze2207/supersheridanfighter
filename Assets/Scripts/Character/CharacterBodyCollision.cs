﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterBodyCollision : MonoBehaviour
{
    // Start is called before the first frame update
    public int m_PlayerNumber;

    public float m_BodyDamage = 20.0f;
    public float m_ProjectileDamage = 10.0f;

    public void OnTriggerEnter(Collider trigger)
    {
        // if player got hit by hit part and is not currently being hit
        if ((trigger.tag == "HitPart" || trigger.tag == "HitProjectile") && trigger.gameObject.transform.root != gameObject.transform.root && !gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("hit") && !gameObject.GetComponent<CharacterHealth>().m_isDead)
        {
            // lose hp
            if (trigger.tag == "HitPart")
            {
                gameObject.GetComponent<CharacterHealth>().TakeDamage(m_BodyDamage);
            }
            else if (trigger.tag == "HitProjectile")
            {
                gameObject.GetComponent<CharacterHealth>().TakeDamage(m_ProjectileDamage);
            }
            
            // play hit animation
            gameObject.GetComponent<CharacterActions>().GotHit();
   
        }
    }

    void Awake()
    {
        
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
       
    }
}
