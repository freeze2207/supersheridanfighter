﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterSounds : MonoBehaviour
{
    public int m_PlayerNumber;

    private AudioSource m_Audio;

    // Start is called before the first frame update
    void Start()
    {
        m_Audio.clip = null;
        m_Audio.playOnAwake = false;
    }

    private void OnEnable()
    {
        m_Audio = gameObject.GetComponent<AudioSource>();
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlayImpact()
    {
        System.Random random = new System.Random((int)Math.Round(Time.time * 1000) % 10);
        m_Audio.clip = Resources.Load<AudioClip>("Sounds/Impact" + random.Next(1, 3));
        m_Audio.Play();
    }

    public void PlayDie()
    {
        System.Random random = new System.Random((int)Math.Round(Time.time * 1000) % 10);
        m_Audio.clip = Resources.Load<AudioClip>("Sounds/Die" + random.Next(1, 3));
        m_Audio.Play();
    }
    public void PlayLand()
    {
        System.Random random = new System.Random((int)Math.Round(Time.time * 1000) % 10);
        m_Audio.clip = Resources.Load<AudioClip>("Sounds/Land" + random.Next(1, 3));
        m_Audio.Play();
    }
    public void PlayTakeDamage()
    {
        System.Random random = new System.Random((int)Math.Round(Time.time * 1000) % 10);
        m_Audio.clip = Resources.Load<AudioClip>("Sounds/TakeDamage" + random.Next(1, 3));
        m_Audio.Play();
    }
    public void PlayTaunt()
    {
        System.Random random = new System.Random((int)Math.Round(Time.time * 1000) % 10);
        m_Audio.clip = Resources.Load<AudioClip>("Sounds/Taunt" + random.Next(1, 3));
        m_Audio.Play();
    }
}
