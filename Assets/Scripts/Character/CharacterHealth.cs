﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterHealth : MonoBehaviour
{

    public Slider m_Slider;
    public Image m_FillImage;
    public Color m_FullHealthColor = Color.green;       // The color the health bar will be when on full health.
    public Color m_ZeroHealthColor = Color.red;
    public int m_PlayerNumber;
    public bool m_isDead;
    
    public float m_CurrentHealth;

    private float m_MaxHealth = 100f;

    private void OnEnable()
    {
        // When the player is enabled, reset the player's health and whether or not it's dead.
        m_CurrentHealth = m_MaxHealth;
        m_isDead = false;

        m_Slider = GameObject.Find("HealthBarPlayer" + m_PlayerNumber).GetComponent<Slider>();
        m_FillImage = m_Slider.transform.GetChild(1).GetChild(0).gameObject.GetComponent<Image>();
        // Update the health slider's value and color.
        SetHealthUI();
    }

    public void TakeDamage(float amount)
    {
        // Reduce current health by the amount of damage done.
        m_CurrentHealth -= amount;

        // Change the UI elements appropriately.
        SetHealthUI();

        // If the current health is at or below zero and it has not yet been registered, call OnDeath.
        if (m_CurrentHealth <= 0f && !m_isDead)
        {
            OnDeath();
        }
    }

    private void SetHealthUI()  // implement later
    {
        // Set the slider's value appropriately.
        m_Slider.value = m_CurrentHealth;

        // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
        m_FillImage.color = Color.Lerp(m_ZeroHealthColor, m_FullHealthColor, m_CurrentHealth / m_MaxHealth);
    }

    private void OnDeath()
    {
        // Set the flag so that this function is only called once.
        m_isDead = true;

        //Play dead animation
        gameObject.GetComponent<CharacterActions>().OnDeath();
        

    }

    public void PlayerSetInactive()
    {
        gameObject.SetActive(false);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
