﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class CharacterManager
{
    public Transform m_SpawnPoint;
    [HideInInspector] public int m_PlayerNumber;
    [HideInInspector] public string m_PlayerText;
    [HideInInspector] public GameObject m_Instance;
    [HideInInspector] public int m_Wins;
    [HideInInspector] public string m_CharacterName;

    private CharacterActions m_CharacterActions;
    private CharacterHealth m_CharacterHealth;
    private CharacterBodyCollision m_CharacterBodyCollision;
    private CharacterSounds m_CharacterSounds;

    public void Setup()
    {
        m_CharacterActions = m_Instance.GetComponent<CharacterActions>();
        m_CharacterHealth = m_Instance.GetComponent<CharacterHealth>();
        m_CharacterBodyCollision = m_Instance.GetComponent<CharacterBodyCollision>();
        m_CharacterSounds = m_Instance.GetComponent<CharacterSounds>();
        

        if (m_CharacterActions != null && m_CharacterHealth != null && m_CharacterBodyCollision != null && m_CharacterSounds != null)
        {
            m_CharacterActions.m_PlayerNumber = m_PlayerNumber;
            m_CharacterHealth.m_PlayerNumber = m_PlayerNumber;
            m_CharacterBodyCollision.m_PlayerNumber = m_PlayerNumber;
            m_CharacterSounds.m_PlayerNumber = m_PlayerNumber;
        }

        m_PlayerText = "Player " + m_PlayerNumber;
    }

    public void DisableControl()
    {
        if (m_CharacterActions) m_CharacterActions.enabled = false;
    }

    public void EnableControl()
    {
        if (m_CharacterActions) m_CharacterActions.enabled = true;
    }

    public void OpenSounds()
    {
        m_CharacterSounds.enabled = true;
    }

    public void OffSounds()
    {
        m_CharacterSounds.enabled = false;
    }

    public void Reset()
    {
        m_Instance.transform.position = m_SpawnPoint.position;
        m_Instance.transform.rotation = m_SpawnPoint.rotation;

        m_Instance.SetActive(false);
        m_Instance.SetActive(true);
    }
}
