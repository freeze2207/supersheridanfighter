﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
    private float m_LeftLockPosition = -7f;
    private float m_RightLockPosition = 7f;
    public GameObject m_LeftWall;
    public GameObject m_RightWall;

    public GameObject m_Player1Instance;
    public GameObject m_Player2Instance;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void MoveBothWalls()
    {
        Vector3 newWallPosition = gameObject.transform.position;
        m_LeftWall.transform.position = newWallPosition + new Vector3(-3.645f, 0, 0);
        m_RightWall.transform.position = newWallPosition + new Vector3(3.645f, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {   
        // Players are moving same direction
        if (Input.GetAxis("Horizontal1") * Input.GetAxis("Horizontal2") > 0 && GameObject.Find("GamePlayManager").GetComponent<GamePlayManager>().m_TimeText.text != "99")
        {
            Vector3 newCameraPosition = gameObject.transform.position;
            if (newCameraPosition.x + Input.GetAxis("Horizontal1") * Time.deltaTime > m_LeftLockPosition && newCameraPosition.x + Input.GetAxis("Horizontal1") * Time.deltaTime < m_RightLockPosition)
            {
                newCameraPosition.x += Input.GetAxis("Horizontal1") * Time.deltaTime;
                gameObject.transform.position = newCameraPosition;
                MoveBothWalls();
            }
            
        }
    }
}
