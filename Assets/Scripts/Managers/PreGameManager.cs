﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PreGameManager : MonoBehaviour
{

    public Text m_message;
    public Text m_Player1Selection;
    public Text m_Player2Selection;
    public GameObject m_NinjaButton;
    public GameObject m_WarriorButton;
    [HideInInspector] public GameObject m_Player1Prefab;
    [HideInInspector] public GameObject m_Player2Prefab;
    [HideInInspector] public PlayerSelectionManager m_PlayerSelection;
    private GameObject m_SplashScreen;
    private GameObject m_SelectionScreen;


    private bool m_Player1Ready = false;
    private bool m_Player2Ready = false;
    
    
    // Start is called before the first frame update
    void Start()
    {
        m_SplashScreen = GameObject.Find("SplashPanel");
        m_SelectionScreen = GameObject.Find("SelectionPanel");

        m_SelectionScreen.SetActive(false);
        m_PlayerSelection = GameObject.Find("PlayerSelection").GetComponent<PlayerSelectionManager>();
    }

    private void ActivateSelectionScreen()
    {
        m_SplashScreen.SetActive(false);
        m_SelectionScreen.SetActive(true);
    }

    private void LoadFightScreen()
    {
        SceneManager.LoadScene(1);
    }
    private bool isAllPlayersReady()
    {
        return m_Player1Ready && m_Player2Ready;
    }

    private void PlayersSelection()
    {
        if(EventSystem.current.currentSelectedGameObject == null)
        {
            EventSystem.current.SetSelectedGameObject(m_NinjaButton);
        }
        if (!m_Player1Ready)
        {
            m_message.text = string.Empty;
            m_message.text = "Player 1 Select\nuse A or J (keyboard) to select";

            if (Input.GetButtonUp("Select") && EventSystem.current.currentSelectedGameObject != null)
            {
                if (EventSystem.current.currentSelectedGameObject.name.Contains("Ninja"))
                {
                    m_Player1Prefab = Resources.Load<GameObject>("Prefabs/NinjaControl");
                    m_Player1Selection.text = "Ninja";
                }
                else if (EventSystem.current.currentSelectedGameObject.name.Contains("Warrior"))
                {
                    m_Player1Prefab = Resources.Load<GameObject>("Prefabs/WarriorControl");
                    m_Player1Selection.text = "Warrior";
                }
                m_Player1Ready = true;
            }
        }
        else if (!m_Player2Ready)
        {
            m_message.text = string.Empty;
            m_message.text = "Player 2 Select\nuse A or J(keyboard) to select";

            if (Input.GetButtonUp("Select") && EventSystem.current.currentSelectedGameObject != null)
            {
                if (EventSystem.current.currentSelectedGameObject.name.Contains("Ninja"))
                {
                    m_Player2Selection.text = "Ninja";
                    if (m_Player2Selection.text == m_Player1Selection.text)
                    {
                        m_Player2Prefab = Resources.Load<GameObject>("Prefabs/NinjaControl_Variant");
                    }
                    else
                    {
                        m_Player2Prefab = Resources.Load<GameObject>("Prefabs/NinjaControl");
                    }
                    
                }
                else if (EventSystem.current.currentSelectedGameObject.name.Contains("Warrior"))
                {
                    m_Player2Selection.text = "Warrior";
                    if (m_Player2Selection.text == m_Player1Selection.text)
                    {
                        m_Player2Prefab = Resources.Load<GameObject>("Prefabs/WarriorControl_Variant");
                    }
                    else
                    {
                        m_Player2Prefab = Resources.Load<GameObject>("Prefabs/WarriorControl");
                    }
                }
                m_Player2Ready = true;
            }
        }
        

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetButtonUp("Submit"))
        {
            ActivateSelectionScreen();
        }
        if (m_SelectionScreen.activeSelf)
        {
            PlayersSelection();
        }
        if (isAllPlayersReady())
        {
            m_message.text = "Press Enter/Start to Fight!\nA - Attack\nB - Block\nX - Throw\nY - Taunt!";
        }
        if (isAllPlayersReady() && Input.GetButtonUp("Submit"))
        {
            m_PlayerSelection.m_Player1Prefab = m_Player1Prefab;
            m_PlayerSelection.m_Player2Prefab = m_Player2Prefab;
            LoadFightScreen();
        }
    }
}
