﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayManager : MonoBehaviour
{
    public static GamePlayManager gamePlayManager;
    public int m_NumberRoundsToWin = 3;
    private float m_StartDelay = 3f;
    private float m_EndDelay = 3f;
    public Text m_MessageText;
    public Text m_Player1Text;
    public Text m_Player2Text;
    public Text m_TimeText;

    public float m_RoundTime = 100.0f;
    [HideInInspector] public GameObject m_Player1Prefab;
    [HideInInspector] public GameObject m_Player2Prefab;
    public CharacterManager m_Player1;
    public CharacterManager m_Player2;

    private int m_RoundNumber;
    private WaitForSeconds m_StartWait;
    private WaitForSeconds m_EndWait;
    private CharacterManager m_RoundWinner;
    private CharacterManager m_GameWinner;
    private Vector3 m_CameraLocation;
    private Vector3 m_LeftWallLocation;
    private Vector3 m_RightWallLocation;

    private float m_RoundStartTime;

    private PlayerSelectionManager m_PlayerSelectionManager;

    void Awake()
    {
        if (gamePlayManager == null)
        {
            gamePlayManager = this;
        }
        else
        {
            Destroy(this);
        }

        if(GameObject.Find("PlayerSelection") != null && GameObject.Find("PlayerSelection").GetComponent<PlayerSelectionManager>() != null)
        {
            m_PlayerSelectionManager = GameObject.Find("PlayerSelection").GetComponent<PlayerSelectionManager>();
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        if(m_PlayerSelectionManager != null)
        {
            m_Player1Prefab = m_PlayerSelectionManager.m_Player1Prefab;
            m_Player2Prefab = m_PlayerSelectionManager.m_Player2Prefab;
        }
        

        if(m_Player1Prefab == null || m_Player2Prefab == null)
        {
            Debug.Log("Into the Debug mode or at least One of the Player Prefab is null!");
            m_Player1Prefab = Resources.Load<GameObject>("Prefabs/NinjaControl");
            m_Player2Prefab = Resources.Load<GameObject>("Prefabs/WarriorControl");
        }

        SpawnPlayers();
        m_CameraLocation = GameObject.FindGameObjectWithTag("MainCamera").transform.position;
        m_LeftWallLocation = GameObject.Find("StageBorderLeft").transform.position;
        m_RightWallLocation = GameObject.Find("StageBorderRight").transform.position;

        StartCoroutine(GameLoop());
    }

    private void SpawnPlayers()
    {
        
        m_Player1.m_Instance = Instantiate(m_Player1Prefab, m_Player1.m_SpawnPoint.position, m_Player1.m_SpawnPoint.rotation) as GameObject;
        m_Player2.m_Instance = Instantiate(m_Player2Prefab, m_Player2.m_SpawnPoint.position, m_Player2.m_SpawnPoint.rotation) as GameObject;

        m_Player1.m_PlayerNumber = 1;
        m_Player1.m_CharacterName = m_Player1Prefab.name.Contains("Ninja") ? "Ninja" : "Warrior";
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraManager>().m_Player1Instance = m_Player1.m_Instance;
        m_Player1.Setup();
  
        m_Player2.m_PlayerNumber = 2;
        m_Player2.m_CharacterName = m_Player2Prefab.name.Contains("Ninja") ? "Ninja" : "Warrior";
        GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraManager>().m_Player2Instance = m_Player2.m_Instance;
        m_Player2.Setup();
    }

    private IEnumerator GameLoop()
    {
        // Start off by running the 'RoundStarting' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundStarting());

        // Once the 'RoundStarting' coroutine is finished, run the 'RoundPlaying' coroutine but don't return until it's finished.
        yield return StartCoroutine(RoundPlaying());

        // Once execution has returned here, run the 'RoundEnding' coroutine, again don't return until it's finished.
        yield return StartCoroutine(RoundEnding());

        // This code is not run until 'RoundEnding' has finished.  At which point, check if a game winner has been found.
        if (m_GameWinner != null)
        {
            // If there is a game winner, game over scene
            if(m_PlayerSelectionManager != null)
            {
                m_PlayerSelectionManager.m_GameWinner = "Player " + m_GameWinner.m_PlayerNumber;
            }

            SceneManager.LoadScene(2);
        }
        else
        {
            // If there isn't a winner yet, restart this coroutine so the loop continues.
            // Note that this coroutine doesn't yield.  This means that the current version of the GameLoop will end.
            StartCoroutine(GameLoop());
        }
    }

    private IEnumerator RoundStarting()
    {
        // As soon as the round starts reset the players and make sure they can't move.
        ResetAllPlayers();
        DisablePlayerControl();
        TurnOffPlayersSound();
        // reset Camera
        GameObject.FindGameObjectWithTag("MainCamera").transform.position = m_CameraLocation;
        GameObject.Find("StageBorderLeft").transform.position = m_LeftWallLocation;
        GameObject.Find("StageBorderRight").transform.position = m_RightWallLocation;

        // Increment the round number and display text showing the players what round it is.
        m_RoundNumber++;
        m_MessageText.text = "ROUND " + m_RoundNumber;
        m_Player1Text.text = m_Player1.m_CharacterName + ": " + m_Player1.m_Wins + " Wins";
        m_Player2Text.text = m_Player2.m_CharacterName + ": " + m_Player2.m_Wins + " Wins";
        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
        // As soon as the round begins playing let the players control.
        EnablePlayerControl();
        TurnOnPlayersSound();

        m_RoundStartTime = Time.time;
        // Clear the text from the screen.
        m_MessageText.text = string.Empty;
         
        // While there is not one player left...
        while (!OnePlayerLeft() && Convert.ToInt32(m_TimeText.text) > 0)
        {
            // ... return on the next frame.
            yield return null;
        }
    }


    private IEnumerator RoundEnding()
    {
        // Stop players from moving.
        DisablePlayerControl();
        TurnOffPlayersSound();
        
        // Clear the winner from the previous round.
        m_RoundWinner = null;

        // See if there is a winner now the round is over.
        m_RoundWinner = GetRoundWinner();

        // If there is a winner, increment their score, record the round time
        if (m_RoundWinner != null)
        {
            m_RoundWinner.m_Wins++;

        }


        // Now the winner's score has been incremented, see if someone has won the game.
        m_GameWinner = GetGameWinner();


        // Get a message based on the scores and whether or not there is a game winner and display it.
        string message = EndMessage();
        m_MessageText.text = message;

        // Wait for the specified length of time until yielding control back to the game loop.
        yield return m_EndWait;
    }

    private bool OnePlayerLeft()
    {
        // If there are one or fewer player remaining return true, otherwise return false.
        return m_Player1.m_Instance.activeSelf ^ m_Player2.m_Instance.activeSelf;
    }

    private CharacterManager GetRoundWinner()
    {
        if (Convert.ToInt32( m_TimeText.text) == 0)
        {
            return m_Player1.m_Instance.GetComponent<CharacterHealth>().m_CurrentHealth > m_Player2.m_Instance.GetComponent<CharacterHealth>().m_CurrentHealth ? m_Player1 : m_Player2;
        }
        if (OnePlayerLeft())
        {
            return m_Player1.m_Instance.activeSelf ? m_Player1 : m_Player2;
        }

        
        // If none of the players are active it is a draw so return null.
        return null;
    }

    private CharacterManager GetGameWinner()
    {
        if (m_Player1.m_Wins == m_NumberRoundsToWin)
        {
            return m_Player1;
        }
        if (m_Player2.m_Wins == m_NumberRoundsToWin)
        {
            return m_Player2;
        }

        // If no players have enough rounds to win, return null.
        return null;
    }

    private string EndMessage()
    {
        // By default when a round ends there are no winners so the default end message is a draw.
        string message = "DRAW!";

        // If there is a winner then change the message to reflect that.
        if (m_RoundWinner != null)
            message = m_RoundWinner.m_PlayerText + " WINS THE ROUND!";

        m_Player1Text.text = m_Player1.m_CharacterName + ": " + m_Player1.m_Wins + " Wins";
        m_Player2Text.text = m_Player2.m_CharacterName + ": " + m_Player2.m_Wins + " Wins";

        // If there is a game winner, change the entire message to reflect that.
        if (m_GameWinner != null)
            message = m_GameWinner.m_PlayerText + " WINS THE GAME!";

        return message;
    }

    private void ResetAllPlayers()
    {
        m_Player1.Reset();
        m_Player2.Reset();
    }

    private void EnablePlayerControl()
    {
        m_Player1.EnableControl();
        m_Player2.EnableControl();
    }

    private void DisablePlayerControl()
    {
        m_Player1.DisableControl();
        m_Player2.DisableControl();
    }

    private void TurnOffPlayersSound()
    {
        m_Player1.OffSounds();
        m_Player2.OffSounds();
    }

    private void TurnOnPlayersSound()
    {
        m_Player1.OpenSounds();
        m_Player2.OpenSounds();
    }
    private void UpdateClock(float time)
    {
        if (m_RoundStartTime != 0 && time > 0)
        {
            m_TimeText.text = Math.Floor(time).ToString();
        }
        else if (time < 0)
        {
            // Judge the winner by hp
        }
        else
        {
            m_TimeText.text = m_RoundTime.ToString();
        }

    }
    // Update is called once per frame
    void Update()
    {
        if(!OnePlayerLeft() && m_MessageText.text == string.Empty)
        {
            UpdateClock(m_RoundTime - (Time.time - m_RoundStartTime));
        }
        
        GetRoundWinner();
 
    }
}
