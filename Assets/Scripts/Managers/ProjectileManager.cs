﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour
{
    // Start is called before the first frame update
    private float m_LifeSpan = 7f;
    private AudioSource m_Audio;
    private void OnEnable()
    {
        m_Audio = gameObject.GetComponent<AudioSource>();
        Invoke("DeferredDestroy", m_LifeSpan);
    }

    private void OnTriggerEnter(Collider trigger)
    {
        if (trigger.gameObject != gameObject.transform.root.gameObject)
        {
            if (gameObject.tag == "HitProjectile" && trigger.tag == "HitProjectile")
            {
                GameObject.Find("Ground").GetComponent<AudioSource>().Play();
            }
            
            DeferredDestroy();
        }    
    }

    void Start()
    {
        if(gameObject.name.Contains("Skull"))
        {
            m_Audio.clip = Resources.Load<AudioClip>("Sounds/SkullProjectile");
            m_Audio.Play();
        }
        else
        {
            m_Audio.clip = Resources.Load<AudioClip>("Sounds/StarProjectile");
            m_Audio.Play();
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void DeferredDestroy()
    {
        Destroy(this.gameObject);
    }
}
