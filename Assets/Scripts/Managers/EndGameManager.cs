﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndGameManager : MonoBehaviour
{
    public Text m_Message;
    public GameObject m_StartAgainButton;

    private PlayerSelectionManager m_GameResultManager;
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.Find("PlayerSelection") != null && GameObject.Find("PlayerSelection").GetComponent<PlayerSelectionManager>() != null)
        {
            m_GameResultManager = GameObject.Find("PlayerSelection").GetComponent<PlayerSelectionManager>();
        }

        if(m_GameResultManager != null)
        {
            m_Message.text = m_GameResultManager.m_GameWinner + " is the Winner!";
        }
        else
        {
            m_Message.text = "No Results";
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (EventSystem.current.currentSelectedGameObject == null)
        {
            EventSystem.current.SetSelectedGameObject(m_StartAgainButton);
        }

        if (Input.GetButtonUp("Submit") && EventSystem.current.currentSelectedGameObject != null)
        {
            if (EventSystem.current.currentSelectedGameObject.name.Contains("Again"))
            {
                StartNewGame();
            }else if (EventSystem.current.currentSelectedGameObject.name.Contains("Quit"))
            {
                QuitGame();
            }
        }
    }
    public void StartNewGame()
    {
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
