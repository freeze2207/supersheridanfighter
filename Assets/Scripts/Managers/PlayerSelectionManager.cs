﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSelectionManager : MonoBehaviour
{
    
    [HideInInspector] public GameObject m_Player1Prefab;
    [HideInInspector] public GameObject m_Player2Prefab;
    [HideInInspector] public string m_GameWinner;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(gameObject);
    }


}
